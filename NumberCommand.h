//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_NUMBERCOMMAND_H
#define DATAEAST_CALC_NUMBERCOMMAND_H

#include "Command.h"

class NumberCommand : public Command {
    int number;
public:
    explicit NumberCommand(int number) : number(number){
        priority = 0;
    };

    void execute(std::stack<int> &stack) const override {
        stack.push(number);
    }
};

#endif //DATAEAST_CALC_NUMBERCOMMAND_H
