//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_PLUSCOMMAND_H
#define DATAEAST_CALC_PLUSCOMMAND_H

#include "Command.h"

class PlusCommand : public Command {
public:
    PlusCommand() { priority = 1; };

    void execute(std::stack<int> &stack) const override {
        int a, b;

        a = stack.top();
        stack.pop();

        b = stack.top();
        stack.pop();

        stack.push(a + b);
    }
};

#endif //DATAEAST_CALC_PLUSCOMMAND_H
