//
// Created by 214 on 14.03.2020.
//

#include "Calculator.h"
#include "NumberCommand.h"
#include "CommandFactory.h"

int Calculator::calculate(std::string &expression) {
    std::list<std::shared_ptr<Command>> list;
    parseString(expression, list);
    return calculate(list);
}

int Calculator::calculate(std::list<std::shared_ptr<Command>> &commandList) {
    if (commandList.empty())
        return 0;

    std::stack<int> stack;

    for (const auto &command : commandList) {
        (*command).execute(stack);
    }

    return stack.top();
}

void Calculator::parseString(std::string &string, std::list<std::shared_ptr<Command>> &list) {
    class CommandOrBracket {

        /* Этот класс существует только для использования в стеке в этой функции.
         * При парсинге с учётом скобок и приоритета используется временный стек,
         * в котором хранятся как операции, так и скобки, которые операциями не
         * являются. Чтобы не расширять обязанности класса Command используется
         * этот вложенный класс.
         */

        std::shared_ptr<Command> cmd;
        bool brckt;

        CommandOrBracket(std::shared_ptr<Command> command, bool bracket) : cmd(command), brckt(bracket) {}

    public:
        static CommandOrBracket command(std::shared_ptr<Command> command) {
            return CommandOrBracket(std::move(command), false);
        }

        static CommandOrBracket bracket() {
            return CommandOrBracket(nullptr, true);
        }

        bool isBracket() {
            return brckt;
        }

        std::shared_ptr<Command> getCommandPtr() {
            return cmd;
        }

        int getPriority() {
            if (brckt)
                return 0;
            else
                return (*cmd).getPriority();
        }

    };

    std::stack<CommandOrBracket> stack;

    for (int i = 0; i < string.length(); i++) {
        char c = string[i];

        if (c <= '9' && c >= '0') {
            int number = c - '0';

            for (i; i + 1 < string.length() && string[i + 1] <= '9' && string[i + 1] >= '0'; i++) {
                number *= 10;
                number += (string[i + 1] - '0');
            }

            list.emplace_back(std::make_shared<NumberCommand>(number));
        } else {
            if (c == '(') {
                stack.push(CommandOrBracket::bracket());
            } else if (c == ')') {
                while (!stack.empty() && !stack.top().isBracket()) {
                    list.push_back(stack.top().getCommandPtr());
                    stack.pop();
                }

                if (stack.empty())
                    throw std::runtime_error("Expression has open bracket");

                stack.pop();
            } else {
                std::string name(1, c);

                // Парсит имя операции. В имя входят любые символы, кроме цифр и скобок

                for (i; i + 1 < string.length() && (string[i + 1] > '9' || string[i + 1] < '0')
                        && string[i + 1] != '(' && string[i + 1] != ')'; i++)
                    name.append(1, string[i + 1]);

                std::shared_ptr<Command> command = CommandFactory::createCommand(name);

                while (!stack.empty() && stack.top().getPriority() >= (*command).getPriority()) {
                    list.push_back(stack.top().getCommandPtr());
                    stack.pop();
                }
                stack.push(CommandOrBracket::command(command));
            }
        }
    }
    while (!stack.empty()) {
        if (!stack.top().isBracket()) {
            list.push_back(stack.top().getCommandPtr());
            stack.pop();
        }
        else
            throw std::runtime_error("Expression has open bracket");
    }
}