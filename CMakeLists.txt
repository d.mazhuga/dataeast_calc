cmake_minimum_required(VERSION 3.12)
project(dataeast_calc)

set(CMAKE_CXX_STANDARD 14)

add_executable(dataeast_calc main.cpp Calculator.cpp Calculator.h Command.h NumberCommand.h PlusCommand.h MinusCommand.h MultiplyCommand.h DivideCommand.h CommandFactory.cpp CommandFactory.h)