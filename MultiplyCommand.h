//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_MULTIPLYCOMMAND_H
#define DATAEAST_CALC_MULTIPLYCOMMAND_H

#include "Command.h"

class MultiplyCommand : public Command {
public:
    MultiplyCommand() { priority = 2; };

    void execute(std::stack<int> &stack) const override {
        int a, b;

        a = stack.top();
        stack.pop();

        b = stack.top();
        stack.pop();

        stack.push(a * b);
    }
};

#endif //DATAEAST_CALC_MULTIPLYCOMMAND_H
