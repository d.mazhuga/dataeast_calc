//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_DIVIDECOMMAND_H
#define DATAEAST_CALC_DIVIDECOMMAND_H

#include <stdexcept>
#include "Command.h"

class DivideCommand : public Command {
public:
    DivideCommand() { priority = 2; };

    void execute(std::stack<int> &stack) const override {
        int a, b;

        a = stack.top();
        stack.pop();

        b = stack.top();
        stack.pop();

        if (a == 0)
            throw std::runtime_error("Division by zero");

        stack.push(b / a);
    }
};

#endif //DATAEAST_CALC_DIVIDECOMMAND_H
