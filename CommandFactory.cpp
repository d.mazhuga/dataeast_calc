//
// Created by 214 on 14.03.2020.
//

#include "CommandFactory.h"

std::unique_ptr<Command> CommandFactory::createCommand(std::string name) {
    if (name == "+")
        return std::make_unique<PlusCommand>();
    else if (name == "-")
        return std::make_unique<MinusCommand>();
    else if (name == "*")
        return std::make_unique<MultiplyCommand>();
    else if (name == "/")
        return std::make_unique<DivideCommand>();
    else
        throw std::runtime_error("Unknown symbol \"" + name + "\"");
}
