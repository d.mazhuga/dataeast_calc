#include <iostream>
#include <vector>
#include "Command.h"
#include "NumberCommand.h"
#include "MultiplyCommand.h"
#include "PlusCommand.h"
#include "Calculator.h"

int main() {
    std::string s;
    int result;

    std::cout << "Enter expression: ";
    std::cin >> s;

    try {
        result = Calculator::calculate(s);
        std::cout << "Result: " << result << std::endl;
    }
    catch (std::runtime_error &e) {
        std::cerr << "An error occurred: " << e.what() << std::endl;
    }

    return 0;
}