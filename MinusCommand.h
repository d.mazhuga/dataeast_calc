//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_MINUSCOMMAND_H
#define DATAEAST_CALC_MINUSCOMMAND_H

#include "Command.h"

class MinusCommand : public Command {
public:
    MinusCommand() { priority = 1; };

    void execute(std::stack<int> &stack) const override {
        int a, b;

        a = stack.top();
        stack.pop();

        b = stack.top();
        stack.pop();

        stack.push(b - a);
    }
};

#endif //DATAEAST_CALC_MINUSCOMMAND_H
