//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_CALCULATOR_H
#define DATAEAST_CALC_CALCULATOR_H

#include <string>
#include <stack>
#include <list>
#include <memory>
#include "Command.h"

class Calculator {
    static void parseString(std::string &string, std::list<std::shared_ptr<Command>> &list);

public:
    static int calculate(std::string &expression);

    static int calculate(std::list<std::shared_ptr<Command>> &commandList);
};


#endif //DATAEAST_CALC_CALCULATOR_H
