//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_COMMAND_H
#define DATAEAST_CALC_COMMAND_H

#include <stack>

class Command {
protected:
    int priority = 0;
public:
    virtual ~Command() = default;

    virtual void execute(std::stack<int> &stack) const = 0;

    int getPriority() { return priority; }
};

#endif //DATAEAST_CALC_COMMAND_H
