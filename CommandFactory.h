//
// Created by 214 on 14.03.2020.
//

#ifndef DATAEAST_CALC_COMMANDFACTORY_H
#define DATAEAST_CALC_COMMANDFACTORY_H

#include <memory>

#include "Command.h"
#include "PlusCommand.h"
#include "MinusCommand.h"
#include "MultiplyCommand.h"
#include "DivideCommand.h"

class CommandFactory {
public:
    static std::unique_ptr<Command> createCommand(std::string name);
};


#endif //DATAEAST_CALC_COMMANDFACTORY_H
